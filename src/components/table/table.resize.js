import { $ } from '@core/dom';

export function resizeHandler($root, event) {
  const table = event.target.closest('.excel__table');
  const tableHeight = parseInt(getComputedStyle(table).height);
  const tableWidth = parseInt(getComputedStyle(table).width);

  const $resizer = $(event.target);
  const $parent = $resizer.closest('[data-type="resizable"]');
  const columnHeight = parseInt(getComputedStyle($parent.$el).height);
  const columnWidth = parseInt(getComputedStyle($parent.$el).width);
  const columnMinWidth = parseInt(getComputedStyle($parent.$el).minWidth);

  const coords = $parent.getCoords();
  const type = $resizer.data.resize;
  const typeProp = type === 'col' ? 'bottom' : 'right';
  let newWidth = 0;
  let newHeight = 0;

  $resizer.$el.classList.add('active');
  $resizer.css({
    [typeProp]: type === 'col' ? -tableHeight + 'px' : -tableWidth + 'px',
  });

  const cells = $root.findAll(`[data-col="${$parent.data.col}"]`);

  document.onmousemove = e => {
    if (type === 'col') {
      const delta = e.pageX - coords.right;
      newWidth = delta + coords.width;

      if (delta < -(columnWidth - columnMinWidth)) {
        newWidth = 40;
      }

      if (delta > -(columnWidth - columnMinWidth)) {
        $resizer.css({ right: -delta + 'px' });
      }
    } else {
      const delta = e.pageY - coords.bottom;
      newHeight = delta + coords.height;
      if (delta < -(columnHeight - 24)) {
        newHeight = 24;
      }

      if (delta > -(columnHeight - 24)) {
        $resizer.css({ bottom: -delta + 'px' });
      }
    }
  };

  document.onmouseup = () => {
    document.onmousemove = null;
    document.onmouseup = null;

    if (type === 'col') {
      cells.forEach(el => (el.style.width = newWidth + 'px'));
    } else {
      $parent.css({ height: newHeight + 'px' });
    }

    $resizer.$el.classList.remove('active');
    $resizer.css({ bottom: 0, right: 0 });
  };
}
