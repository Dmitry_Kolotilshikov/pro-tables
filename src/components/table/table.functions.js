import { range } from '@core/utils';

export const shouldResize = event => {
  return event.target.dataset.resize;
};

export const isCell = event => {
  return event.target.dataset.type === 'cell';
};

export const matrix = ($target, $current) => {
  const target = $target.id(true);
  const current = $current.id(true);

  const cols = range(current.col, target.col);
  const rows = range(current.row, target.row);

  return cols.reduce((res, col) => {
    rows.forEach(row => res.push(`${row}:${col}`));
    return res;
  }, []);
};

export const nextSelector = (key, { row, col }) => {
  switch (key) {
    case 'Enter':
    case 'ArrowDown':
      row++;
      break;
    case 'Tab':
    case 'ArrowRight':
      col++;
      break;
    case 'ArrowLeft':
      col--;
      break;
    case 'ArrowUp':
      row--;
      break;
  }
  if (row < 0 ) row = 0;
  if (row > 14 ) row = 14; // 14 is magic number
  if (col < 0 ) col = 0;
  if (col > 25 ) col = 25; // 25 is magic number

  return `[data-id="${row}:${col}"]`;
};
