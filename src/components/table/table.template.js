const CODES = {
  A: 65,
  Z: 90,
};

// function createCell(letter, idx = '', col) {
//   return `
//     <div class="cell" contenteditable data-col="${col}">
//       ${letter}${idx}
//     </div>
//   `;
// }

function createCell(row = '') {
  return function(letter, col = '') {
    return `
    <div 
    class="cell" 
    contenteditable 
    data-col="${col}" 
    data-id="${row}:${col}"
    data-type="cell"
    >
      ${letter} ${row}
    </div>
  `;
  };
}

function createCol(letter, index) {
  return `
    <div class="column" data-type="resizable" data-col="${index}">
      ${letter}
      <div class="col-resize" data-resize="col"></div>
    </div>
  `;
}

function createRow(content, info = '') {
  return `
    <div class="row" ${info && `data-type="resizable"`}>
    
    <div class="row-info">
      ${info}
      ${info && `<div class="row-resize" data-resize="row"></div>`}
    </div>

    <div class="row-data">${content}</div>   
    
    </div>
  `;
}

function toChar(_, idx) {
  return String.fromCharCode(CODES.A + idx);
}

export function createTable(rowsCount = 15) {
  const colsCount = CODES.Z - CODES.A + 1;
  const rows = [];
  const cols = new Array(colsCount)
      .fill('')
      .map(toChar)
      .map(createCol)
      .join('')
      .trim();

  rows.push(createRow(cols));

  for (let row = 0; row < rowsCount; row++) {
    const cells = new Array(colsCount)
        .fill('')
        .map(toChar)
        // .map((letter, idx) => createCell(letter, row + 1, idx))
        .map(createCell(row))
        .join('')
        .trim();
    rows.push(createRow(cells, row + 1));
  }

  return rows.join('');
}
